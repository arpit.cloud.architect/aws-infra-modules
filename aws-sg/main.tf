provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}
# Module version https://github.com/terraform-aws-modules/terraform-aws-security-group v4.3.0