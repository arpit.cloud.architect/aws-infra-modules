output "vpc_id" {
  value = aws_vpc.main
}

output "private_subnet_ids" {
  value = aws_subnet.private
}

output "public_subnet_ids" {
  value = aws_subnet.public
}

output "nat_subnet_id" {
  value = aws_nat_gateway.gateway
}