
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile

  ignore_tags {
    key_prefixes = ["kubernetes.io/"]
  }
}

locals {
  common_tags = {
    Stack       = var.stack_name
    Environment = var.environment
  }
}