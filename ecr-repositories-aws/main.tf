
provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

output "repositories" {
  value = aws_ecr_repository.repositories
}
