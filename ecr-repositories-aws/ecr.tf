resource "aws_ecr_repository" "repositories" {
  for_each = var.repositories
  name     = format("%s/%s", each.value.stack, each.value.app)
  tags = {
    Stack               = each.value.stack
    App                 = each.value.app
  }
  image_tag_mutability = lookup(each.value, "image_tag_mutability", "MUTABLE")
  image_scanning_configuration {
    scan_on_push = lookup(each.value, "scan_on_push", "true")
  }
}

resource "aws_ecr_repository_policy" "repositories" {
  for_each   = var.repositories
  repository = format("%s/%s", each.value.stack, each.value.app)
  policy     = lookup(var.repositories[each.key], "permissions", var.ecr_permissions)
  depends_on = [aws_ecr_repository.repositories]
}
