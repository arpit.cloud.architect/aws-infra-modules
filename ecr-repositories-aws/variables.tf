variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

variable "stack_name" {
  type        = string
  description = "Name for the stack"
  default     = "ecs-cluster"
}

variable "environment" {
  type        = string
  description = "Environment of the deploy"
}

variable "repositories" {
  type = map(any)
}

variable "ecr_permissions" {
  type = string
}
