# Resource: aws_eks_node_group
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/eks_node_group

resource "aws_eks_node_group" "nodes_general" {
  # Name of the EKS Cluster.
  cluster_name = aws_eks_cluster.eks.name

  # Name of the EKS Node Group.
  node_group_name = "nodes-general-${var.eks_cluster_name}"

  # Amazon Resource Name (ARN) of the IAM Role that provides permissions for the EKS Node Group.
  node_role_arn = aws_iam_role.nodes_general.arn

  # Identifiers of EC2 Subnets to associate with the EKS Node Group. 
  # These subnets must have the following resource tag: kubernetes.io/cluster/CLUSTER_NAME 
  # (where CLUSTER_NAME is replaced with the name of the EKS Cluster).
  subnet_ids = var.private_subnet_ids

  # Configuration block with scaling settings
  scaling_config {
    # Desired number of worker nodes.
    desired_size = var.autoscaling-config.desired_size

    # Maximum number of worker nodes.
    max_size = var.autoscaling-config.max_size

    # Minimum number of worker nodes.
    min_size = var.autoscaling-config.min_size
  }

  # Type of Amazon Machine Image (AMI) associated with the EKS Node Group.
  # Valid values: AL2_x86_64, AL2_x86_64_GPU, AL2_ARM_64
  ami_type = var.autoscaling-config.ami_type

  # Type of capacity associated with the EKS Node Group. 
  # Valid values: ON_DEMAND, SPOT
  capacity_type = var.autoscaling-config.capacity_type

  # Disk size in GiB for worker nodes
  disk_size = var.autoscaling-config.disk_size

  # Force version update if existing pods are unable to be drained due to a pod disruption budget issue.
  force_update_version = var.force_update_version

  # List of instance types associated with the EKS Node Group
  instance_types = var.autoscaling-config.instance_types

  labels = {
    role = "nodes-general"
  }

  # Kubernetes version
  version = var.autoscaling-config.version



  # Ensure that IAM Role permissions are created before and deleted after EKS Node Group handling.
  # Otherwise, EKS will not be able to properly delete EC2 Instances and Elastic Network Interfaces.
  depends_on = [
    aws_iam_role_policy_attachment.amazon_cost_explorer_access_policy,
  ]

}
