resource "aws_iam_user" "useradd" {
  count         = length(var.readonlyusername)
  name          = format("%s-%s", element(var.readonlyusername, count.index), "${var.eks_cluster_name}")
  force_destroy = true
  lifecycle {
    create_before_destroy = false
  }
}

resource "aws_iam_policy_attachment" "readonly-users" {
  count      = length(var.readonlyusername)
  name       = "readonly-users-attach"
  users      = [element(aws_iam_user.useradd.*.name, count.index)]
  policy_arn = aws_iam_policy.eks-readonly.arn
  depends_on = [
    aws_iam_policy.eks-readonly
  ]
}


resource "local_file" "user-arn-list" {
  content  = jsonencode(aws_iam_user.useradd.*.arn)
  filename = "eks-cluster-user-arn"
  depends_on = [
    aws_iam_policy_attachment.readonly-users
  ]
}