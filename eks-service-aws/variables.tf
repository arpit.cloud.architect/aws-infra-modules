variable "aws_region" {
  type = string
}

variable "aws_profile" {
  type = string
}

variable "eks_cluster_name" {
  type        = string
  description = "Name of EKS cluster"
}

variable "autoscaling-config" {
  type = object({
    desired_size   = number
    max_size       = number
    min_size       = number
    ami_type       = string
    capacity_type  = string
    disk_size      = number
    instance_types = list(string)
    version        = string
  })
  description = "EKS specific configuration"
}
variable "readonlyusername" {

  type        = list(string)
  description = "List of readonly user for EKS separated by commas"

}


variable "adminusername" {

  type        = list(string)
  description = "List of admin user for EKS separated by commas"

}

variable "private_subnet_ids" {
  type        = list(string)
  description = "List of all private subnets id"
}

variable "public_subnet_ids" {
  type        = list(string)
  description = "List of all private subnets id"
}

variable "iam_eks_execution_role" {
  type = object({
    name     = string
    policies = map(string)
  })
  default = {
    name = "lifebit-eks-execution-role"
    policies = {
      amazon_eks_worker_node_policy_general   = "arn:aws:iam::aws:policy/AmazonEKSWorkerNodePolicy"
      amazon_eks_cni_policy_general           = "arn:aws:iam::aws:policy/AmazonEKS_CNI_Policy"
      amazon_ec2_container_registry_read_only = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
      amazon_ec2_s3_bucket_access_policy      = "arn:aws:iam::aws:policy/AmazonS3FullAccess"
      amazon_ssm_access_policy                = "arn:aws:iam::aws:policy/AmazonSSMReadOnlyAccess"
      amazon_ec2_full_access_policy           = "arn:aws:iam::aws:policy/AmazonEC2FullAccess"
      amazon_batch_full_access_policy         = "arn:aws:iam::aws:policy/AWSBatchFullAccess"
      amazon_iam_read_access_policy           = "arn:aws:iam::aws:policy/IAMReadOnlyAccess"
    }
  }
}

variable "force_update_version" {
  type    = string
  default = false
}

variable "private_access" {
  type        = string
  default     = false
  description = "Private access to EKS endpoint"
}

variable "public_access" {
  type        = string
  default     = true
  description = "Public access to EKS endpoint"
}

variable "allowed_cidr" {
  type        = list(string)
  default     = []
  description = "List of IP/CIDR allowed to access EKS cluster"
}
