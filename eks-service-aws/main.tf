provider "aws" {
  region  = var.aws_region
  profile = var.aws_profile
}

locals {
  all_subnets = concat(var.private_subnet_ids, var.public_subnet_ids)
}

output "endpoint" {
  value = aws_eks_cluster.eks.endpoint
}