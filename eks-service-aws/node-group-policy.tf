# Resource: aws_iam_role
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role

# Create IAM role for EKS Node Group

data "aws_iam_policy_document" "ec2_assume" {
  statement {
    actions = ["sts:AssumeRole"]

    principals {
      type        = "Service"
      identifiers = ["ec2.amazonaws.com"]
    }
  }
}

resource "aws_iam_role" "nodes_general" {
  # The name of the role
  name = "IAM-IaC-EKS-${var.eks_cluster_name}"

  # The policy that grants an entity permission to assume the role.
  assume_role_policy = data.aws_iam_policy_document.ec2_assume.json
}

# Resource: aws_iam_role_policy_attachment
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role_policy_attachment

resource "aws_iam_role_policy_attachment" "node_group_policy" {
  for_each   = var.iam_eks_execution_role.policies
  policy_arn = each.value
  role       = aws_iam_role.nodes_general.name
}


resource "aws_iam_policy" "amazon_cost_explorer" {

  name = "eks-cost-explorer-${var.eks_cluster_name}"
  policy = jsonencode(

    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Sid" : "CloudOSCostExplorer",
          "Effect" : "Allow",
          "Action" : [
            "ce:GetCostAndUsage",
            "ce:GetTags",
            "pricing:GetProducts"
          ],
          "Resource" : "*"
        }
      ]
    }
  )



}

resource "aws_iam_role_policy_attachment" "amazon_cost_explorer_access_policy" {
  # The ARN of the policy you want to apply.
  # https://github.com/SummitRoute/aws_managed_policies/blob/master/policies/IAMReadOnlyAccess
  policy_arn = aws_iam_policy.amazon_cost_explorer.arn

  # The role the policy should be applied to
  role = aws_iam_role.nodes_general.name

}