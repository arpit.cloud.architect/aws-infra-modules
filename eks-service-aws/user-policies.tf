resource "aws_iam_policy" "eks-readonly" {

  name = "eks-readonly-${var.eks_cluster_name}"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "eks:DescribeNodegroup",
            "eks:ListNodegroups",
            "eks:DescribeCluster",
            "eks:ListClusters",
            "eks:AccessKubernetesApi",
            "ssm:GetParameter",
            "eks:ListUpdates",
            "eks:ListFargateProfiles"
          ],
          "Resource" : "*"
        }
      ]
  })
}


resource "aws_iam_policy" "eks-admin" {

  name = "eks-admin-${var.eks_cluster_name}"
  policy = jsonencode(
    {
      "Version" : "2012-10-17",
      "Statement" : [
        {
          "Effect" : "Allow",
          "Action" : [
            "*"
          ],
          "Resource" : "*"
        }
      ]
  })
}


