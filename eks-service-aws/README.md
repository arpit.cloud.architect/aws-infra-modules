# eks-cluster
 This is terraform code for EKS cluster setup
# Install kubectl, iam-authenticator on system with aws-cli configured
 # Once cluster setup is done 
 aws eks --region <region> update-kubeconfig --name <cluster-name> --profile <aws-profile>
