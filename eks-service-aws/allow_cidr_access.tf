resource "aws_security_group_rule" "allowed_cidr" {
  count             = var.private_access ? 1 : 0
  type              = "ingress"
  protocol          = "-1"
  from_port         = 0
  to_port           = 0
  cidr_blocks       = var.allowed_cidr
  security_group_id = aws_eks_cluster.eks.vpc_config[0].cluster_security_group_id
}