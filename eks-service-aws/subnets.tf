#This will add comman tag which defines that whether subnet should be shared with multiple EKS cluster
resource "aws_ec2_tag" "common_tags" {
  count       = length(local.all_subnets)
  resource_id = element(local.all_subnets, count.index)
  key         = "kubernetes.io/cluster/${var.eks_cluster_name}"
  value       = "shared"
}
#To allow public load balancer to choose public subnet
resource "aws_ec2_tag" "tagging_public" {
  count       = length(var.public_subnet_ids)
  resource_id = element(var.public_subnet_ids, count.index)
  key         = "kubernetes.io/role/elb"
  value       = "1"
}
#To allow internal load balancer to choose private subnet 
resource "aws_ec2_tag" "tagging_private" {
  count       = length(var.private_subnet_ids)
  resource_id = element(var.private_subnet_ids, count.index)
  key         = "kubernetes.io/role/internal-elb"
  value       = "1"
}